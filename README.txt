CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
Instant Feedback provides an ajax form that gives the user a simple yes/no question (ie. "Did you find what you where looking for?") No-sayers get a open follow-up question. The results are exposed to views and anything that can deal with flags.


 * For a full description of the module, visit the project page:
   https://drupal.org/sandbox/oskarwastensson/2484571


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/2484571


REQUIREMENTS
------------
This module requires the following modules:
 * Views (https://drupal.org/project/views)
 * Flag (https://www.drupal.org/project/flag)
 * Entity API (https://www.drupal.org/project/entity)

RECOMMENDED MODULES
-------------------
 * Session API (https://www.drupal.org/project/session_api):
   When enabled, feedback is available for guests (anonymous users).

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:

   - Administer Instant Feedback

     Allows access to the Instant Feedback configuration form and reset functions.

   - View Instant Feedback

     Allows access to statistics pages.

   - Give Instant Feedback

     Users in roles with the "Give Instant Feedback" permission will see the feedback form.


 * Set what entities and bundles should be targets of instant feedback at Administration » Structure » Instant Feedback » Settings


MAINTAINERS
-----------
Current maintainers:
 * Oskar Wastensson (OWast) https://drupal.org/user/1129208

This project has been developed by:
 * Happiness. Happiness is an Internet consultant agency, working with Internet and multimedia production. Happiness' focus is always usability and user friendliness, both in design and system development. Visit http://www.happiness.se for more information.

This project har been sponsored by:
 * Swedish Inheritance Fund (Allmänna arvsfonden.) A Swedish State fund, established in 1928. The purpose of the fund is to support non-profit organizations and other voluntary associations to help improve conditions for children, young people and the disabled. Visit http://www.arvsfonden.se for more information.


