<?php
/**
 * @file
 * Default template for Instant Feedback Stats for a given entity
 *
 * Available variables:
 *
 * $question        string  The question asked
 * $yes             int     Number of answers yes
 * $no              int     Number of answers no
 * $total           int     Number of answers in total
 * $percent_yes     int     Percent of answers yes
 * $percent_no      int     Percent of ansers no
 * $sentence_yes    string  A string representation of positive answers
 * $sentence_no     string  A string representation of negative answers
 * $sentence_total  string  A string reprentation of the toal amount of answers
 *
 * $follow_up_question string  The follow up question asked
 * $followups          array   An array of strings containg answers to the follow-up question
 */
?>

<div class="instant-feedback-results">
  <div class="instant-feedback-data">
    <h4><?php print $question ?></h4>
    <div class="instant-feedback-yes"><?php print $sentence_yes ?></div>
    <div class="instant-feedback-no"><?php print $sentence_no ?></div>
    <div class="instant-feedback-total"><?php print $sentence_total ?></div>
  </div>
  <?php if ( !empty($followups) ) : ?>
    <h4><?php print $follow_up_question ?></h4>
    <div class="instant-feedback-followups">
    <?php foreach ( $followups as $followup ) : ?>
      <div class="instant-feedback-followup-item <?php print $followup['type'] ?>">
        <?php if ( $followup['type'] == 'instant_feedback_revision' ) : ?>
          <span class="instant-feedback-revision-log"><?php print $followup['label'] ?>:</span>
        <?php endif; ?>
        <em class="instant-feedback-followup-date"><?php print $followup['date'] ?></em><br />
        <span class="instant-feedback-followup-text"><?php print $followup['text'] ?></span>
      </div>
    <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>