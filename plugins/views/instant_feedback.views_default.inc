<?php
/**
 * @file
 * Default views for Instant Feedback Statistics Overview
 *
 */

function instant_feedback_views_default_views() {
  $views = array();

  // Create one view for each entity type
  foreach ( entity_get_info() as $entity_type => $entity_info ) {
    $bundles = array_keys(field_info_bundles($entity_type));

    foreach ( $bundles as $bundle ) {
      if ( variable_get("instant_feedback_{$entity_type}_{$bundle}") ) {
        if ( isset($entity_info['entity keys']['id']) && isset($entity_info['entity keys']['label']) ) {
          // View will fail if flags havn't been created. This makes sure flags exist.
          _instant_feedback_get_flag($entity_type, 'yes');
          _instant_feedback_get_flag($entity_type, 'no');

          // Add view
          $views[] = _instant_feedback_admin_view($entity_type, $entity_info);

          // Views are bundle agnostic, so we only need to do this once per entity type
          break;
        }
      }
    }
  }
  return $views;
}

/**
 * Defines a default view for instant feedback stats for given entity type
 * @param  string $type Entity type
 * @param  array  $info Entity info as given in entity_get_info()
 * @return array        Default view
 */
function _instant_feedback_admin_view($type, $info) {

  $label = $info['entity keys']['label'];

  /* Instant Feedback Stats view */
  $view = new view();
  $view->name = 'instant_feedback_stats_' . $type;
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = $info['base table'];
  $view->human_name = t('Instant Feedback Stats for @entity_label', array('@entity_label' => $info['label']));
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = t('Instant Feedback for @entity_label', array('@entity_label' => $info['label']));
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'count' => 'count',
    'count_1' => 'count_1',
  );

  $handler->display->display_options['style_options']['default'] = 'count';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'count' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'count_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );

  /* Relationship: Flags: if_node_no counter */
  $handler->display->display_options['relationships']['flag_count_rel']['id'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['table'] = $info['base table'];
  $handler->display->display_options['relationships']['flag_count_rel']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['label'] = t('No');
  $handler->display->display_options['relationships']['flag_count_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_count_rel']['flag'] = "if_{$type}_no";
  /* Relationship: Flags: if_node_yes counter */
  $handler->display->display_options['relationships']['flag_count_rel_1']['id'] = 'flag_count_rel_1';
  $handler->display->display_options['relationships']['flag_count_rel_1']['table'] = $info['base table'];
  $handler->display->display_options['relationships']['flag_count_rel_1']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel_1']['label'] = t('Yes');
  $handler->display->display_options['relationships']['flag_count_rel_1']['required'] = 0;
  $handler->display->display_options['relationships']['flag_count_rel_1']['flag'] = "if_{$type}_yes";
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = $info['base table'];
  $handler->display->display_options['fields']['id']['field'] = $info['entity keys']['id'];
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = $info['base table'];
  $handler->display->display_options['fields']['title']['field'] = $info['entity keys']['label'];
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = "admin/structure/instant_feedback/$type/[id]";
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['fields']['count']['label'] = t('Nos');
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count_1']['id'] = 'count_1';
  $handler->display->display_options['fields']['count_1']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count_1']['field'] = 'count';
  $handler->display->display_options['fields']['count_1']['relationship'] = 'flag_count_rel_1';
  $handler->display->display_options['fields']['count_1']['label'] = t('Yes\'s');
  /* Field: Global: Math expression */
  $handler->display->display_options['fields']['expression']['id'] = 'expression';
  $handler->display->display_options['fields']['expression']['table'] = 'views';
  $handler->display->display_options['fields']['expression']['field'] = 'expression';
  $handler->display->display_options['fields']['expression']['label'] = '= Yes - No';
  $handler->display->display_options['fields']['expression']['precision'] = '0';
  $handler->display->display_options['fields']['expression']['expression'] = '[count_1] - [count]';
  /* Sort criterion: No Flag counter */
  $handler->display->display_options['sorts']['count']['id'] = 'count';
  $handler->display->display_options['sorts']['count']['table'] = 'flag_counts';
  $handler->display->display_options['sorts']['count']['field'] = 'count';
  $handler->display->display_options['sorts']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['sorts']['count']['ui_name'] = 'No Flag counter';
  $handler->display->display_options['sorts']['count']['order'] = 'DESC';

  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'OR',
  );

  if ( $type = 'node' ) {
    $handler->display->display_options['filter_groups']['groups'][2] = 'AND';
  }

  /* Filter criterion: Flags: Flag counter */
  $handler->display->display_options['filters']['count']['id'] = 'count';
  $handler->display->display_options['filters']['count']['table'] = 'flag_counts';
  $handler->display->display_options['filters']['count']['field'] = 'count';
  $handler->display->display_options['filters']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['filters']['count']['operator'] = '>';
  $handler->display->display_options['filters']['count']['value']['value'] = '0';
  $handler->display->display_options['filters']['count']['group'] = 1;
  /* Filter criterion: Flags: Flag counter */
  $handler->display->display_options['filters']['count_1']['id'] = 'count_1';
  $handler->display->display_options['filters']['count_1']['table'] = 'flag_counts';
  $handler->display->display_options['filters']['count_1']['field'] = 'count';
  $handler->display->display_options['filters']['count_1']['relationship'] = 'flag_count_rel_1';
  $handler->display->display_options['filters']['count_1']['operator'] = '>';
  $handler->display->display_options['filters']['count_1']['value']['value'] = '0';
  $handler->display->display_options['filters']['count_1']['group'] = 1;

  if ($type == 'node') {
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = 1;
    $handler->display->display_options['filters']['status']['group'] = 2;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  }

  return $view;
}