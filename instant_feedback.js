  /* Global Drupal */
/* Global jQuery */

(function($) {
  "use strict";
  $.fn.instantFeedback = function (op) {
    var $form = $(this);
    var $flags = $form.find('[data-instant-feedback-flags]');
    var $followUp = $form.find('[data-instant-feedback-follow-up]');

    $form.removeClass('instant-feedback-answer-yes');
    $form.removeClass('instant-feedback-followup');
    $form.removeClass('instant-feedback-question');

    switch (op) {
      case "yes":
        $form.html(Drupal.t('Thank you for your feedback!'));
        $form.addClass('instant-feedback-answer-yes');
        $followUp.hide();
        break;
      case "no":
        $flags.hide();
        $followUp.show();
        $form.addClass('instant-feedback-followup');
        break;
      case "create":
      default:
        if ( $followUp.hasClass('hidden') ) {
          $form.addClass('instant-feedback-question');
        }
        else {
          $form.addClass('instant-feedback-followup');
        }
    }
  }

  $(document).bind('flagGlobalAfterLinkUpdate', function(event, data) {
    var info = data.flagName.split('_');
    var $form = $(data.link).parents('[data-instant-feedback-form]');

    if ( info[0] === 'if' ) {
      $form.instantFeedback(info[2]);
    }
  });

  Drupal.behaviors.instantFeedback = {
    attach: function(context) {
      $('[data-instant-feedback-form]', context).instantFeedback();
    }
  }

}(jQuery));