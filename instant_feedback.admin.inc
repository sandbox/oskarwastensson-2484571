<?php

/**
 * @file
 * Administrative Pages for Instant Feedback module
 *
 */

/**
 * Callback function for administration menu item
 */
function instant_feedback_admin_settings($form, &$form_state) {

  $ignore_entity_types = array(
    'search_api_index',
    'search_api_server',
    'search_api_autocomplete_search',
    'redirect',
    'flagging',
    'og_membership',
    'og_membership_type',
    'rules_config',
  );

  $form['instant_feedback_question'] = array(
    '#title' => t('Question'),
    '#description' => t('A question with the vistior will give a yes or no answer to.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('instant_feedback_question', t('Did you find what you where looking for?')),
  );

  $form['instant_feedback_followup_question'] = array(
    '#title' => t('Follow up question title'),
    '#description' => t('The title of the followup-question.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('instant_feedback_followup_question', t('What where you looking for?')),
  );

  $form['instant_feedback_followup_description'] = array(
    '#title' => t('Follow-up question description'),
    '#description' => t('A more wordy description of the followup-question.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('instant_feedback_followup_description', t('Tell us how we can improve the page so you can find the information you where looking for.')),
  );

  $form['instant_feedbeck_followup_more_details'] = array(
    '#title' => t('Follow up question - more details'),
    '#description' => t('You can add even more details about your followup here - this is displayed below the input.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('instant_feedbeck_followup_more_details', ''),
  );

  // Generate a fieldset for each entity type
  foreach ( entity_get_info() as $entity_type => $entity_info ) {
    if ( in_array($entity_type, $ignore_entity_types) ) {
      continue;
    }

    $form[$entity_type] = array(
      '#type' => 'fieldset',
      '#title' => t('Enable feedback on @entity_label', array('@entity_label' => $entity_info['label'])),
      '#collapsible' => TRUE,
      '#collapsed' => ($entity_type != 'node'),
    );

    // Generate a checkbox for each bundle
    foreach ( field_info_bundles($entity_type) as $bundle => $info ) {
      $setting_name = "instant_feedback_{$entity_type}_{$bundle}";
      $form[$entity_type][$setting_name] = array(
        '#type' => 'checkbox',
        '#title' => $info['label'],
        '#default_value' => variable_get($setting_name, FALSE),
      );
    }

  }
  return system_settings_form($form);
}

/**
 * Reset form for deleting flags on a given entity
 *
 * @param  array  $form
 * @param  array  &$form_state
 * @param  string $entity_type Entity type (node)
 * @param  int    $entity_id   Id of the entity
 * @return array               Form array
 */
function instant_feedback_reset_form($form, &$form_state, $entity_type, $entity_id) {
  $entity = entity_load_single($entity_type, $entity_id);

  $entity_label = entity_label($entity_type, $entity);

  $form['info'] = array(
    '#markup' => t('<p>If the content of <em>@label</em> (@entity_type @entity_id) has been significantly altered, previous feedback might no longer be relevant. If this is the case you can clear old data by clicking the button below. This action is not reversible.</p>', array(
        '@label' => $entity_label,
        '@entity_type' => $entity_type,
        '@entity_id' => $entity_id,
      )
    ),
  );

  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );

  $form['#instant_feedback_reset'] = array(
    'entity_type' => $entity_type,
    'entity_id' => $entity_id,
  );

  return $form;
}


/**
 * Submit handler for reset form.
 *
 * @see instant_feedback_reset_form()
 */
function instant_feedback_reset_form_submit(&$form, $form_state) {

  $entity_type = $form['#instant_feedback_reset']['entity_type'];
  $entity_id = $form['#instant_feedback_reset']['entity_id'];

  $yes = _instant_feedback_get_flag($entity_type, 'yes');
  $no = _instant_feedback_get_flag($entity_type, 'no');

  flag_reset_flag($yes, $entity_id);
  flag_reset_flag($no, $entity_id);

  drupal_set_message(t('All Instant Feedback on @entity_type @entity_id has been deleted.', array(
    '@entity_type' => $entity_type,
    '@entity_id' => $entity_id
    )
  ));
}