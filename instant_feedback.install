<?php

/**
 * @file
 * Update functions
 */

/**
 * Implements hook_schema().
 */
function instant_feedback_schema() {

  $schema = array();

  $schema['instant_feedback_path'] = instant_feedback_path_schema();

  return $schema;
}


function instant_feedback_path_schema() {
  return array(
    'description' => 'The base table for the Path entity',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key of the Path entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'path' => array(
        'description' => 'Path.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
  );
}


/**
 * Implements hook_uninstall().
 */
function instant_feedback_uninstall() {

  variable_del('instant_feedback_question');
  variable_del('instant_feedback_followup_question');
  field_delete_field('field_instant_feedback_followup');

  foreach ( entity_get_info() as $entity_type => $entity_info ) {
    $short_type = substr($entity_type, 0, 24);

    if ( $flag = flag_get_flag("if_{$short_type}_yes") ) {
      watchdog('instant_feedback', 'Flag @name has been deleted.', array('@name' => $flag->name), WATCHDOG_NOTICE);
      $flag->delete();
    }

    if ( $flag = flag_get_flag("if_{$short_type}_no") ) {
      watchdog('instant_feedback', 'Flag @name has been deleted.', array('@name' => $flag->name), WATCHDOG_NOTICE);
      $flag->delete();
    }

    foreach ( field_info_bundles($entity_type) as $bundle => $info ) {
      variable_del("instant_feedback_{$entity_type}_{$bundle}");
    }
  }
}

/**
 * Deletes old feedback followup field
 */
function instant_feedback_update_7006() {
  field_delete_field("field_instant_feedback_followup");
  field_purge_batch(10);
}

/**
 * Adds path entity schema
 */
function instant_feedback_update_7007() {
  db_create_table('instant_feedback_path', instant_feedback_path_schema());
}

/**
 * Removes old default view
 */
function instant_feedback_update_7008() {
  if ( $view = views_get_view('instant_feedback_stats') ) {
    $view->delete();
  }
}
